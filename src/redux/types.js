﻿/** Starships */
// export const STARSHIP_GET = '[Starship] GET';
export const FETCH_STARSHIPS_BEGIN   = 'FETCH_STARSHIPS_BEGIN';
export const FETCH_STARSHIPS_SUCCESS = 'FETCH_STARSHIPS_SUCCESS';
export const FETCH_STARSHIPS_FAILURE = 'FETCH_STARSHIPS_FAILURE';

/** Films */
export const FETCH_FILMS_BEGIN   = 'FETCH_FILMS_BEGIN';
export const FETCH_FILMS_SUCCESS = 'FETCH_FILMS_SUCCESS';
export const FETCH_FILMS_FAILURE = 'FETCH_FILMS_FAILURE';
