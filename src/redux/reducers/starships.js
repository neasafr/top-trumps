﻿import * as types from '../types';

const initialState = {
  data: [],
  loading: false,
  error: null,
};

export default function(state = initialState, { type, payload }) {
  switch (type) {
    case types.FETCH_STARSHIPS_BEGIN:
      return { ...state, loading: true, error: null };
    case types.FETCH_STARSHIPS_SUCCESS:
      return { ...state, loading: false, data: payload.starships };
    case types.FETCH_STARSHIPS_FAILURE:
      return { ...state, loading: false, error: payload.error, data: [] };
    default:
      return state;
  }
}
