import React, { Component } from 'react';
import PropTypes from 'prop-types';
// Components
import ScoreBoard from '../ScoreBoard/ScoreBoard';
import Card from '../Card/Card';
// Styles
import './Game.css';

class Game extends Component {
    constructor(props) {
        super(props);

        this.state = {
            cards: {
                user: [],
                comp: []
            },
            activeCat: '',
            cardDraw: 0
        }

        this.selectCat = this.selectCat.bind(this);
    }

    componentDidUpdate(prevProps) {
        // set the state from the props received from the API
        const { userCards, compCards } = this.props;

        if ( prevProps !== this.props ) {
            this.setState({ cards: {
                user: userCards, 
                comp: compCards
            }});
        }
    }

    selectCat(category) {
        const { 
            cards: { user, comp },
            cardDraw
        } = this.state;
        // set active category
        this.setState({ activeCat: category });

        // next fire the gameOutcome after a certain amount of time
        if ( user[cardDraw][category] > comp[cardDraw][category] ) {
            alert('You win!');
            // remove first card from comp add to user
        } else if ( comp[cardDraw][category] > user[cardDraw][category] ) {
            alert('You lose!');
            // remove first card from user add to comp
        } else {
            alert('Draw!');
        }
        // increment the cardDraw after a certain amount of time
        // this.setState({ cardDraw: cardDraw + 1 });

    }

    render() {
        const { 
            cards: { user, comp }, 
            activeCat,
            cardDraw
        } = this.state;
        const userScore = user && user.length;
        const compScore = comp && comp.length;

        return (
            <div className="game">
                <ScoreBoard userScore={userScore} compScore={compScore} />
                <div className="deck">
                    { user.length && 
                        <Card 
                            name={user[cardDraw]['name']} 
                            activeCat={activeCat} 
                            cardCats={user[cardDraw]} 
                            onClick={this.selectCat}
                        />
                    }
                    { comp.length && 
                        <Card 
                            name={comp[cardDraw]['name']}  
                            activeCat={activeCat} 
                            cardCats={comp[cardDraw]} 
                            compCard="true"
                        />
                    }
                </div>
            </div>
        );
    }
}

Game.propTypes = {
    userCards: PropTypes.array,
    compCards: PropTypes.array
};

Game.defaultProps = {
    userCards: [],
    compCards: []
};

export default Game;